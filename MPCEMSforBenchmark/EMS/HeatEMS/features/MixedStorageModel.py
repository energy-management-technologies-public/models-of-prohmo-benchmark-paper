import pyomo.core as pyomo
import math
import pyomo.environ as aml


def add_mixed_storage_model(m):
    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # Heat Demand Type
    m.heat_demand_type = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # Heat Demand Type
    m.heat_demand_type = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    indexlist = ['Heating', 'HeatDHW'] + m.Heat_dict["HeatProcess"]
    if 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
        indexlist.remove('ElectricHeater')
    m.heat_pro = pyomo.Set(
        initialize=indexlist,
        doc='Set of processes')

    m.e_pro_heat = pyomo.Var(
        m.tm, m.heat_pro,
        within=pyomo.NonNegativeReals,
        doc='Q_g/d[t] - Heat (kWh) of generation type (e.g. CHP) per timestep [kWh]')

    m.e_heat_sto = pyomo.Var(
        m.t,
        within=pyomo.NonNegativeReals,
        doc='E_sto[t] - heat within the thermal storage')

    m.heat_storage_state = pyomo.Constraint(
        m.tm,
        rule=heat_storage_state_rule,
        doc='E[t] = E[t-1] * (1 - d[l+1]) ** dt) + dE_in/out[t] - dE_loss[t]')

    m.heat_storage_state_max = pyomo.Constraint(
        m.tm,
        rule=heat_storage_state_max_rule,
        doc='E[t] <= E_max')

    m.heat_storage_initial_state = pyomo.Constraint(
        rule=heat_storage_initial_state_rule,
        doc='E[t_0] == storage.init * capacity')

    if not hasattr(m, 'validation'):
        m.heat_storage_state_cyclicity = pyomo.Constraint(
            rule=heat_storage_state_cyclicity_rule,
            doc='E[t_0] <= E[t_end]')
    return m


def mixed_heat_surplus(m, tm, com):
    power_surplus = 0
    if com in m.heat_generation_type:
        power_surplus -= m.e_pro_heat[tm, com.replace('Heat', '')]
    if com in m.heat_demand_type:
        power_surplus += m.e_pro_heat[tm, com]
    return power_surplus


def heat_storage_state_rule(m, tm):
    # E[t] = E[t-1] * (1 - d[l+1]) ** dt) + dE_in/out[t] - dE_loss[t]
    e_in_out = 0
    for pro in m.heat_pro:
        if pro in ['Heating', 'HeatDHW']:
            e_in_out -= m.e_pro_heat[tm, pro]
        else:
            rho = 1  # [kg/l]
            c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
            v = m.Heat_dict["StorageSize"]  # l
            t_sto = m.Heat_dict["T_low"] + m.e_heat_sto[tm - 1] / (rho * v * c_p)
            eff = (7.392 - 0.0762 * t_sto) / m.Heat_dict['EfficiencyNorm'].loc[pro, 'ratio']
            e_in_out += m.e_pro_heat[tm, pro] * eff
    disch = m.Heat_dict['StorageDischarge'].mean()

    return m.e_heat_sto[tm] == m.e_heat_sto[tm - 1] * (1 - disch) ** m.dt.value + e_in_out


def heat_storage_state_max_rule(m, tm):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    v = m.Heat_dict["StorageSize"]  # l
    e_pro_max = rho * c_p * v * (m.Heat_dict["T_high"] - m.Heat_dict["T_low"])
    return m.e_heat_sto[tm] <= e_pro_max


def heat_storage_initial_state_rule(m):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    v = m.Heat_dict["StorageSize"]  # l
    e_0 = rho * c_p * v * (m.Heat_dict['StorageInit'].mean() - m.Heat_dict["T_low"])
    return m.e_heat_sto[m.t[1]] == e_0


def heat_storage_state_cyclicity_rule(m):
    return m.e_heat_sto[m.t[1]] <= m.e_heat_sto[m.t[-1]]


