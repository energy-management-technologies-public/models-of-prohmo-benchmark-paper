import pyomo.core as pyomo
import pyomo.environ as aml
import pandas as pd


def clt_heatstorage(m):
    # Subsets
    m.heat_sto_bin_set = pyomo.Set(
        initialize=m.Heat_dict['HeatProcess'] + ['Heating', 'HeatDHW'],
        doc='Set of binary variables of the storage connections to generators and demand')
    m.heat_sto_conn_bin_set = pyomo.Set(
        initialize=list(dict.fromkeys(m.Heat_dict['ConnectionDemand'].loc[:, 'Port'].tolist() +
                                      m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist())),
        doc='Set of binary variables of the storage connections')
    m.heat_sto_bin_ineq_set = pyomo.Set(
        initialize=['smaller', 'bigger'],
        doc='Set of binary variables for smaller or bigger')

    m.heat_sto_bin_step_set = pyomo.Set(
        initialize=['current', 'previous'],
        doc='Set of binary variables to consider previous and current timestep')

    # Variables
    if m.mode['mip_heatlevels']:
        # if mip_heatlevels is active, heat can be extracted only from certain levels
        # => boolean variables are required
        m.heat_sto_conn_bin = pyomo.Var(
            m.tm, m.heat_sto_conn_bin_set, m.heat_levels,
            within=pyomo.Boolean,
            doc='b_conn[l,t] - Port binary: True if heat level is active')
        m.heat_sto_conn_bin_comb = pyomo.Var(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_levels,
            within=pyomo.Boolean,
            doc='b_s/b comb,conn[l,t] - Binary support variable, required for or configuration')
        m.heat_sto_conn_ineq_bin = pyomo.Var(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            within=pyomo.Boolean,
            doc='b_s/b,conn[l,t] - Binary variable, true if h_cum is bigger or smaller than the storage level height')
        m.heat_sto_ineq_bin_y1 = pyomo.Var(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            within=pyomo.NonNegativeReals,
            doc='y_1,s/b,conn[l,t] - Big-M-Variable for heat_sto_conn_ineq_bin')
        m.heat_sto_ineq_bin_y2 = pyomo.Var(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            within=pyomo.NonNegativeReals,
            doc='y_2,s/b,conn[l,t] - Big-M-Variable for heat_sto_conn_ineq_bin')
        m.heat_sto_bin = pyomo.Var(
            m.tm, m.heat_sto_bin_set, m.heat_levels,
            within=pyomo.Boolean,
            doc='b_x[l,t] - Storage binary for multiple connections: True if heat levels are active')
    else:
        # if mip_heatlevels is not active, boolean variable is set to 1. Heat can be extracted from all levels
        m.heat_sto_bin = pyomo.Param(
            m.tm, m.heat_sto_bin_set, m.heat_levels,
            initialize=1,
            doc='Binary variables deactivated, heat can be extracted from all levels')

    m.heat_sto_conv_in = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='dV_conv,in[l,t] - Heat flow into storage (l) per timestep')
    m.heat_sto_conv_out = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='dV_conv,out[l,t] - Heat flow out of storage (l) per timestep')

    if m.heat_gen_set - m.heat_gen_conv_set:
        m.heat_sto_cond = pyomo.Var(
            m.tm, m.heat_levels,
            within=pyomo.NonNegativeReals,
            doc='dV_cond[l,t] - Volume change caused by conductive heat source (l) per timestep')
    else:
        # if no conductive heat source is available, conductive volume changes are 0
        m.heat_sto_cond = pyomo.Param(
            m.tm, m.heat_levels,
            initialize=0,
            doc='dV_cond[l,t] - Volume change caused by conductive heat source (l) per timestep')

    m.heat_sto_loss = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='dV_loss[l,t] - Heat storage loss per timestep')

    m.heat_sto_mix_in = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='dV_mix,in[l,t] - Mixing flow into the layer')
    m.heat_sto_mix_out_up = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='dV_mix,up[l,t] - Mixing flow out of the layer')
    m.heat_sto_mix_out_down = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='dV_mix,down[l,t] - Mixing flow out of the layer')

    m.heat_sto_con = pyomo.Var(
        m.t, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='V[l,t] - Heat content of storage level (l) in timestep')

    # storage rules
    m.heat_storage_state_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_storage_state_const_rule,
        doc='V[l,t] = V[l,t-1] + dV_conv[l,t] + dV_cond[l,t] + dV_loss[l,t]')
    m.heat_discharge_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_discharge_const_rule,
        doc='dV_loss[l,t] = V[l+1,t-1] * (1 - (1 - d[l+1]) ** dt) - V[l, t-1] * (1 - (1 - d[l]) ** dt)')
    m.heat_sto_cond_const = pyomo.Constraint(
        m.tm, m.heat_levels, (m.heat_gen_set - m.heat_gen_conv_set),
        rule=heat_sto_cond_const_rule,
        doc='dV_cond[l,t] = * tau_cond[l,t] / (rho * c_p * Delta T')
    m.heat_storage_initial_state_const = pyomo.Constraint(
        m.heat_levels,
        rule=heat_storage_initial_state_rule,
        doc='V[l,t_0] == storage.init * capacity')
    if not hasattr(m, 'validation'):
        m.heat_storage_state_const_cyclicity = pyomo.Constraint(
            rule=heat_storage_state_const_cyclicity_rule,
            doc='SOC[t_0] <= SOC[t_end]')

    if m.mode['mip_heatlevels']:
        # active port:
        # h_cum[0,n-1] < h < h_cum [0,n]
        # bigger:
        # h_cum[0,n-1] + y11 - y12 == h                     - m.heat_sto_bin_h_cum_const
        # y11 <= b1                                         - m.heat_sto_ineq_bin_y1_const
        # y12 <= 1 - b1                                     - m.heat_sto_ineq_bin_y2_const
        # smaller:
        # h_cum[0,n] - y12 + y21 == h                       - m.heat_sto_bin_h_cum_const
        # y21 <= b2                                         - m.heat_sto_ineq_bin_y1_const
        # y22 <= 1 - b2                                     - m.heat_sto_ineq_bin_y2_const
        # h_cum1, h_cum2: [0,1], h_cum of levels below or above
        # h: normalized port height
        # y11, y12, y21, y22: NonNegativeReals
        # b, b1, b2: Boolean

        # Output level for heat generation
        m.heat_sto_bin_h_cum_const = pyomo.Constraint(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            rule=heat_sto_bin_h_cum_const_rule,
            doc='h_cum[l,t] +/- y_1,s/b,conn[l,t] -/+ y_2,s/b,conn[l,t] == h_conn; '
                'checks if h_cum is bigger/smaller than storage height')
        m.heat_sto_ineq_bin_y1_const = pyomo.Constraint(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            rule=heat_sto_ineq_bin_y1_const_rule,
            doc='y_1,s/b,conn[l,t] <= b_s/b,conn[l,t]; checks if h_cum is bigger/smaller than storage height')
        m.heat_sto_ineq_bin_y2_const = pyomo.Constraint(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_sto_bin_step_set, m.heat_levels,
            rule=heat_sto_ineq_bin_y2_const_rule,
            doc='y_2,s/b,conn[l,t] <= 1 - b_s/b,conn[l,t]; checks if h_cum is bigger/smaller than storage height')
        m.heat_sto_conn_bin_or_const = pyomo.Constraint(
            m.tm, m.heat_sto_conn_bin_set, m.heat_sto_bin_ineq_set, m.heat_levels,
            rule=heat_sto_conn_bin_or_rule,
            doc='0 <= 2 * b_s/b comb,conn[l,t] - b_s/b comb,conn[l,t] + b_s/b comb,conn[l,t-1] <= 1; or constraint')
        m.heat_sto_conn_bin_and_const = pyomo.Constraint(
            m.tm, m.heat_sto_conn_bin_set, m.heat_levels,
            rule=heat_sto_conn_bin_and_rule,
            doc='0 <= -2 *  b_conn[l,t] +  b_s comb,conn[l,t] +  b_b comb,conn[l,t] <= 1; and constraint')

        # several ports connected to one process
        m.heat_sto_bin_const = pyomo.Constraint(
            m.tm, m.heat_sto_bin_set, m.heat_levels,
            rule=heat_sto_bin_rule,
            doc='true, if heat can be extracted from generation or demand process, multiple ports possible')

    return m


# constraints

# V[l,t] == V[l,t-1] + dV_conv[l,t] + dV_cond[l,t] + dV_mix[l,t] + dV_loss[l,t]
# Volume into or out of storage layer defined by m.heat_vertex
# Discharge happens by conversion into a lower level
def heat_storage_state_const_rule(m, tm, level):
    level = int(level.replace('Level', ''))

    # dV_conv[l,t] = dV_p,in[l,t] - dV_p,out[l,t]
    dv_conv = m.heat_sto_conv_in[tm, 'Level' + str(level)] - m.heat_sto_conv_out[tm, 'Level' + str(level)]

    if level > 0:
        # dV_cond[l,t] = dV_cond,hx[l-1,t] - dV_cond,hx[l,t]
        dv_cond = m.heat_sto_cond[tm, 'Level' + str(level - 1)] - m.heat_sto_cond[tm, 'Level' + str(level)]
    else:
        # dV_cond[0,t] = -dV_cond,hx[0,t] - Lowest level only decreases due to conductive heat exchange
        dv_cond = - m.heat_sto_cond[tm, 'Level' + str(level)]

    # dV_mix[l,t] = dV_mix,in[l,t] -dV_mix,up[l,t] - dV_mix,down[l,t]
    dv_mix = (m.heat_sto_mix_in[tm, 'Level' + str(level)] - m.heat_sto_mix_out_up[tm, 'Level' + str(level)] -
              m.heat_sto_mix_out_down[tm, 'Level' + str(level)])

    return m.heat_sto_con[tm, 'Level' + str(level)] == (m.heat_sto_con[tm - 1, 'Level' + str(level)]
                                                        + dv_conv + dv_cond + dv_mix
                                                        + m.heat_sto_loss[tm, 'Level' + str(level)])


def heat_discharge_const_rule(m, tm, level):
    level = int(level.replace('Level', ''))
    if 'Level' + str(level) == m.heat_levels[1]:
        # lowest level doesn't discharge by converting into a lower level
        dv_loss_plus = (m.heat_sto_con[tm - 1, 'Level' + str(level + 1)] *
                        (1 - (1 - m.Heat_dict['StorageDischarge']['Level' + str(level + 1)]) ** m.dt.value))
        dv_loss_minus = 0
    elif 'Level' + str(level) == m.heat_levels[-1]:
        # highest level doesn't have any gains by discharge from a higher level
        dv_loss_plus = 0
        dv_loss_minus = (m.heat_sto_con[tm - 1, 'Level' + str(level)] *
                         (1 - (1 - m.Heat_dict['StorageDischarge']['Level' + str(level)]) ** m.dt.value))
    else:
        dv_loss_plus = (m.heat_sto_con[tm - 1, 'Level' + str(level + 1)] *
                        (1 - (1 - m.Heat_dict['StorageDischarge']['Level' + str(level + 1)]) ** m.dt.value))
        dv_loss_minus = (m.heat_sto_con[tm - 1, 'Level' + str(level)] *
                         (1 - (1 - m.Heat_dict['StorageDischarge']['Level' + str(level)]) ** m.dt.value))
    return m.heat_sto_loss[tm, 'Level' + str(level)] == dv_loss_plus - dv_loss_minus


def heat_sto_cond_const_rule(m, tm, level, heat_gen):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    delta_t = m.Heat_dict['Delta_T']  # [K]
    if level == m.heat_levels[-1]:
        return m.heat_sto_cond[tm, level] == 0
    else:
        return m.heat_sto_cond[tm, level] == m.tau_heat_gen[tm, heat_gen, level] / (rho * c_p * delta_t)


# initialization of storage content in first timestep t[1]
# forced minimum  storage content in final timestep t[len(m.t)]
# content[t=1] == storage capacity * fraction <= content[t=final]
def heat_storage_initial_state_rule(m, level):
    return m.heat_sto_con[m.t[1], level] == \
           m.Heat_dict['StorageSize'] * m.Heat_dict['StorageInit'][level]


def heat_storage_state_const_cyclicity_rule(m):
    soc_t0 = 0
    soc_end = 0
    for level in m.heat_levels:
        level = int(level.replace('Level', ''))
        delta_t = (m.Heat_dict["T_high"] - m.Heat_dict["T_low"]) / (m.Heat_dict["NumberHeatLevels"] - 1) * level  # [K]
        soc_t0 += m.heat_sto_con[m.t[1], 'Level' + str(level)] * delta_t
        soc_end += m.heat_sto_con[m.t[-1], 'Level' + str(level)] * delta_t
    return soc_t0 <= soc_end


# ensures that heat can only be extracted from the activated level available
def heat_sto_bin_h_cum_const_rule(m, tm, port, ineq_type, step, level):
    # h_cum[0,n-1] < h* < h_cum[0,n]
    # h* = h / h_tot
    #   => smaller: h* < h_cum[0,n]
    #   => bigger:  h* > h_cum[0,n-1]
    dem = m.Heat_dict['ConnectionDemand'].loc[:, 'Port'].tolist()
    gen = m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist()
    gen.sort(reverse=True)
    h_tot = m.Heat_dict['StorageHeight']
    level = int(level.replace('Level', ''))

    if step == 'previous':
        time = tm - 1
    else:
        time = tm

    if ineq_type == 'smaller':
        # h_cum[0, n] - h_cum (storage content) of the levels below and including the current level
        h_cum = sum(m.heat_sto_con[time, 'Level' + str(sto_level)] / (m.Heat_dict['StorageSize'])
                    for sto_level in range(0, level + 1))
    else:
        # h_cum[0, n-1] - h_cum of the levels below the current level
        h_cum = sum(m.heat_sto_con[time, 'Level' + str(sto_level)] / (m.Heat_dict['StorageSize'])
                    for sto_level in range(0, level))

    if port in dem:
        height = (m.Heat_dict['ConnectionDemand'].loc[m.Heat_dict['ConnectionDemand']['Port'] == port]['HeightOut'][0]
                  / h_tot)
    else:
        height = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), port), 'HeightOut'][0] / h_tot

    if ineq_type == 'smaller':
        # h* < h_cum[0,n]
        # h_cum[0,n] - y1 + y2 == h*
        return (h_cum - m.heat_sto_ineq_bin_y1[tm, port, ineq_type, step, 'Level' + str(level)] +
                m.heat_sto_ineq_bin_y2[tm, port, ineq_type, step, 'Level' + str(level)] == height)
    else:
        # h* > h_cum[0,n-1]
        # h_cum[0,n-1] + y1 - y2 == h*
        return (h_cum + m.heat_sto_ineq_bin_y1[tm, port, ineq_type, step, 'Level' + str(level)] -
                m.heat_sto_ineq_bin_y2[tm, port, ineq_type, step, 'Level' + str(level)] == height)


def heat_sto_ineq_bin_y1_const_rule(m, tm, port, ineq_type, step, level):
    # y1 <= b*
    return (m.heat_sto_ineq_bin_y1[tm, port, ineq_type, step, level] <=
            10 * m.heat_sto_conn_ineq_bin[tm, port, ineq_type, step, level])


def heat_sto_ineq_bin_y2_const_rule(m, tm, port, ineq_type, step, level):
    # y2 <= 1 - b*
    return (m.heat_sto_ineq_bin_y2[tm, port, ineq_type, step, level] <=
            10 * (1 - m.heat_sto_conn_ineq_bin[tm, port, ineq_type, step, level]))


def heat_sto_conn_bin_or_rule(m, tm, port, ineq_type, level):
    return aml.inequality(0, 2 * m.heat_sto_conn_bin_comb[tm, port, ineq_type, level] -
                          m.heat_sto_conn_ineq_bin[tm, port, ineq_type, 'current', level] -
                          m.heat_sto_conn_ineq_bin[tm, port, ineq_type, 'previous', level], 1)


def heat_sto_conn_bin_and_rule(m, tm, port, level):
    # and constraint:
    # b = b*_smaller and b*_bigger
    # 0 <= -2 * b + b*_smaller + b*_bigger <= 1
    return aml.inequality(0, - 2 * m.heat_sto_conn_bin[tm, port, level] +
                          m.heat_sto_conn_bin_comb[tm, port, 'smaller', level] +
                          m.heat_sto_conn_bin_comb[tm, port, 'bigger', level], 1)


def heat_sto_bin_rule(m, tm, conn, level):
    # Slack generation can always produce heat
    if conn == 'SlackHeatGen':
        return m.heat_sto_bin[tm, conn, level] == 1

    # if internal electric heater is used, connection is the internal electric heater, else, the connection is the port
    # or heat exchanger of the process
    if conn == 'ElectricHeater' and 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(
            1).tolist():
        conn1 = 'ElectricHeater0'
        if 'ElectricHeater1' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
            conn2 = 'ElectricHeater1'
        else:
            conn2 = float('NaN')
    elif conn in ['Heating', 'HeatDHW']:
        conn1 = m.Heat_dict['ConnectionDemand']['Port'][conn]
        conn2 = float('NaN')
    else:
        conn1 = m.process_dict['connection-low'][conn]
        conn2 = m.process_dict['connection-high'][conn]

    # if only one port is used for the process (conn2 == 'NaN'), use binary of the connection, else use or statement for
    # both binaries
    if pd.isna(conn2):
        return m.heat_sto_bin[tm, conn, level] == m.heat_sto_conn_bin[tm, conn1, level]
    else:
        return aml.inequality(0, 2 * m.heat_sto_bin[tm, conn, level] -
                              m.heat_sto_conn_bin[tm, conn1, level] - m.heat_sto_conn_bin[tm, conn2, level], 1)
