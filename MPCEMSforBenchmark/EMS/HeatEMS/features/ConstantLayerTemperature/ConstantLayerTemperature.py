from .CLT_HeatStorage import *
from .CLT_HeatGenerators import *
from .CLT_HeatConsumption import *


def add_clt_model(m):
    # Heat Levels
    indexlist = []
    i = 0
    while i < m.Heat_dict['NumberHeatLevels']:
        indexlist.append(('Level' + str(i),)[0])
        i += 1
    m.heat_levels = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat levels')

    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # generator
    indexlist = m.Heat_dict["HeatProcess"].copy()
    if 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
        indexlist.remove('ElectricHeater')
    m.heat_gen_set = pyomo.Set(
        initialize=indexlist,
        doc='Set of all generator processes')
    m.heat_pro = pyomo.Set(
        initialize=(indexlist + ['Heating', 'HeatDHW']),
        doc='Set of processes')
    cond = [k for k, v in m.process_dict['connection-low'].items() if 'HeatExchanger' in v]
    for c in cond:
        indexlist.remove(c)
    m.heat_gen_conv_set = pyomo.Set(
        initialize=indexlist,
        doc='Set of heat generators that are convective source')
    m.tau_heat_gen = pyomo.Var(
        m.t, m.heat_gen_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='tau_g[l,t] - Heat flow (l) through process')

    m = clt_heatstorage(m)
    m = clt_heatgenerators(m)
    m = clt_heatconsumption(m)

    m.heat_storage_mix_mass_eq = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_storage_mix_mass_eq_rule,
        doc='dV_mix,in [l+1, t] == dV_mix,up[l, t] - dV_mix,down[l+2, t]')

    m.heat_storage_mix_energy_eq = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_storage_mix_energy_eq_rule,
        doc='# dV_mix,up[l, t] == dV_mix,down[l + 2, t]')

    m.heat_storage_mix_in_out = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_storage_mix_in_rule,
        doc='dV_mix,in[l,t] = mix_coeff * sum(dV_conv,in[l,t]) - sum(dV_conv,out[l,t])')

    m.heat_vertex = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_vertex_conv_rule,
        doc='sum(dV_conv,in[l,t]) == sum(dV_conv,out[l,t] - sum of generation and demand inflows is equal to the '
            'outflows')
    return m


def heat_storage_mix_mass_eq_rule(m, tm, level):
    # dV_mix,in [l+1, t] == dV_mix,up[l, t] - dV_mix,down[l+2, t]
    level = int(level.replace('Level', ''))
    if level < (m.Heat_dict['NumberHeatLevels'] - 2):
        delta_v = (m.heat_sto_mix_out_up[tm, 'Level' + str(level)] +
                   m.heat_sto_mix_out_down[tm, 'Level' + str(level + 2)])
        return m.heat_sto_mix_in[tm, 'Level' + str(level + 1)] == delta_v
    else:
        return m.heat_sto_mix_out_up[tm, 'Level' + str(level)] == 0


def heat_storage_mix_energy_eq_rule(m, tm, level):
    # dV_mix,up[l, t] == dV_mix,down[l + 2, t]
    level = int(level.replace('Level', ''))
    if level > 1:
        return (m.heat_sto_mix_out_up[tm, 'Level' + str(level - 2)] ==
                m.heat_sto_mix_out_down[tm, 'Level' + str(level)])
    else:
        return m.heat_sto_mix_out_down[tm, 'Level' + str(level)] == 0


def heat_storage_mix_in_rule(m, tm, level):
    # dV_mix,in[l,t] = mix_coeff * sum(dV_conv,in[l,t]) - sum(dV_conv,out[l,t])
    level = int(level.replace('Level', ''))
    v = 0
    if 1 <= level <= (m.Heat_dict['NumberHeatLevels'] - 2):
        for heat_gen in m.heat_gen_conv_set:
            for lev in range(level + 1, m.Heat_dict['NumberHeatLevels']):
                v += m.m_heat_gen_conv_out[tm, heat_gen, 'Level' + str(lev)]
            for lev in range(level, m.Heat_dict['NumberHeatLevels']):
                v -= m.m_heat_gen_conv_in[tm, heat_gen, 'Level' + str(lev)]
        return m.heat_sto_mix_in[tm, 'Level' + str(level)] == m.Heat_dict['MixingCoefficient'] * v
    else:
        return m.heat_sto_mix_in[tm, 'Level' + str(level)] == 0


def heat_vertex_conv_rule(m, tm, level):
    # water flow into or out of storage level m =
    #    sum (water flow from heat generation at level m) - sum (water flow to heat generation at level m) +
    #    sum (water flow from heat demand at level m) - sum (water flow to heat demand at level m)
    flow_surplus = 0

    # water flow out of / into storage at level x
    # flow_surplus += m.heat_sto_in_out[tm, level]
    flow_surplus += m.heat_sto_conv_in[tm, level]
    flow_surplus -= m.heat_sto_conv_out[tm, level]

    # water flow out of / into each heat generation process at level x
    for heat_gen in m.heat_gen_conv_set:
        try:
            flow_surplus += m.m_heat_gen_conv_in[tm, heat_gen, level]
        except KeyError:
            pass
        try:
            flow_surplus -= m.m_heat_gen_conv_out[tm, heat_gen, level]
        except KeyError:
            pass

    # water flow out of /into each demand process at level x
    for heat_dem in m.heat_demand_type_set:
        try:
            flow_surplus += m.m_heat_dem_conv_in[tm, heat_dem, level]
        except KeyError:
            pass
        try:
            flow_surplus -= m.m_heat_dem_conv_out[tm, heat_dem, level]
        except KeyError:
            pass

    return flow_surplus == 0


def clt_heat_surplus(m, tm, com):
    # Q_x,tot[t] = sum_(l=0)^(l_max) (Q_x[l,t])
    power_surplus = 0
    if com in m.heat_generation_type:
        power_surplus -= sum(m.e_pro_in_heat[tm, heat_gen, level]
                             for heat_gen, level in m.heat_gen_set * m.heat_levels
                             if heat_gen in com)
    if com in m.heat_demand_type_set:
        power_surplus += sum(m.e_pro_out_heat[tm, com, level]
                             for level in m.heat_levels)
    return power_surplus
